import math
from mpi4py import MPI
import numpy as np
import time


def e_single(n):
    return 1 / float(math.factorial(n))


def e_serijski(n=10):
    return sum(1 / float(math.factorial(i)) for i in range(n))


comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()


if rank == 0:
    print(
        "Aproksimacija broja e sumom reda, pokrenuto sa {0:1d} procesa".format(size))

    n = int(input("Unesite veličinu reda: "))
    pocetak = time.time()
else:
    n = None

n = comm.bcast(n, root=0) 

print("Rank {} je dobio {}".format(rank, n))

results = []

suma_dio = sum(
    1 / float(math.factorial(i))
    for i in range(int(rank * (n / size)), int((rank + 1) * (n / size) + 1))
)
results = comm.gather(suma_dio, root=0)

if rank == 0:
    e = sum(results)
    kraj = time.time()
    print("Rez: {} Vrijeme paralelno {}s".format(e, kraj - pocetak))

    pocetak = time.time()
    e_s = e_serijski(n) 
    kraj = time.time()
    print("Rez: {} Vrijeme serijski {}s".format(e_s, kraj - pocetak))
